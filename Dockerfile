################################################################################
# base system
################################################################################
FROM docker:stable as system

RUN apk add --update --no-cache \
    curl


################################################################################
# builder
################################################################################
FROM system as builder

RUN apk add --update --no-cache \
    --repository http://nl.alpinelinux.org/alpine/edge/testing \
    build-base \
    openblas-dev \
    unzip \
    cmake \
    libtbb libtbb-dev libjpeg-turbo-dev libpng-dev tiff-dev libwebp-dev \
    gcc \
    linux-headers \
    openjdk11 apache-ant \
    python3

ENV JAVA_HOME=/usr/lib/jvm/default-jvm/

ENV OPENCV_VERSION=3.3.0

RUN mkdir -p /opt && cd /opt && \
    curl -fsSLO https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip && \
    unzip ${OPENCV_VERSION}.zip && \
    rm -f ${OPENCV_VERSION}.zip && \
    mkdir -p /opt/opencv-${OPENCV_VERSION}/build && \
    cd /opt/opencv-${OPENCV_VERSION}/build && \
    cmake \
    	  -D CMAKE_BUILD_TYPE=RELEASE \
	  -D CMAKE_INSTALL_PREFIX=/usr/local \
	  -D BUILD_EXAMPLES=NO \
	  -D BUILD_ANDROID_EXAMPLES=NO \
	  -D BUILD_DOCS=NO \
	  -D BUILD_opencv_java=ON \
	  -D BUILD_opencv_python3=ON \
	  .. && \
    make -j"$(nproc)" && \
    make -j"$(nproc)" install && \
    rm -rf /opt/opencv-${OPENCV_VERSION}


################################################################################
# merge
################################################################################
FROM system

RUN echo -e '@testing http://nl.alpinelinux.org/alpine/edge/testing\n\' \
    >> /etc/apk/repositories

RUN apk add --update --no-cache \
    openblas libtbb@testing libjpeg libpng tiff libwebp libgcc \
    tesseract-ocr ttf-dejavu \
    xvfb \
    ffmpeg \
    openjdk11-jre \
    python3

COPY --from=builder /usr/local/ /usr/local/
RUN ln -sf /usr/local/share/OpenCV/java/libopencv_java*.so /usr/lib/libopencv_java.so

RUN mkdir -p /opt && cd /opt/ && \
    curl -fsSL https://launchpad.net/sikuli/sikulix/2.0.0/+download/sikulix-2.0.0.jar -o sikulix.jar && \
    curl -fsSL https://launchpad.net/sikuli/sikulix/2.0.0/+download/sikulixapi-2.0.0.jar -o sikulixapi.jar && \
    curl -fsSLO https://repo1.maven.org/maven2/org/python/jython-standalone/2.7.1/jython-standalone-2.7.1.jar

ENV CLASSPATH=/opt/
