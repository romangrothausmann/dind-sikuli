
# Dockerfile for *SikuliX* based on docker:stable

Screenshot of SikuliX-IDE main screen (from last successful pipeline on master) when run from docker image within GL-CI dind:

![only visible on GL repo homepage](/../-/jobs/artifacts/master/raw/start.png?job=test:ss_start "screenshot of SikuliX-IDE main screen run from docker image")
